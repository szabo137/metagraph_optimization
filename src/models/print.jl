
"""
    show(io::IO, particleValue::ParticleValue)

Pretty print a [`ParticleValue`](@ref), no newlines.
"""
function show(io::IO, particleValue::ParticleValue)
    print(io, "($(particleValue.p), value: $(particleValue.v))")
    return nothing
end
