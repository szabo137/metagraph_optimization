# Manual

## Jupyter Notebooks

In the `notebooks` directory are notebooks containing some examples of the usage of this repository.

- `abc_model_showcase`: A simple showcase of the intended usage of the ABC Model implementation.
