# Scheduler

## Interface
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["scheduler/interface.jl"]
Order   = [:type, :function]
```

## Types
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["scheduler/type.jl"]
Order   = [:type, :function]
```

## Greedy
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["scheduler/greedy.jl"]
Order   = [:type, :function]
```
