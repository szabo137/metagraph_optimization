# Utility

## Helper Functions
```@autodocs
Modules = [MetagraphOptimization]
Pages   = ["./utility.jl"]
Order   = [:type, :function]
```

## Trie Helper
This is a simple implementation of a [Trie Data Structure](https://en.wikipedia.org/wiki/Trie) to greatly improve the performance of the Node Reduction search.

```@autodocs
Modules = [MetagraphOptimization]
Pages   = ["trie.jl"]
Order   = [:type, :function]
```
