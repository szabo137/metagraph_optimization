# Models

## Interface

The interface that has to be implemented for a model to be usable is defined in `src/models/interface.jl`.

```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/interface.jl"]
Order = [:type, :constant, :function]
```

```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/print.jl"]
Order = [:function]
```

## ABC-Model

### Types
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/abc/types.jl"]
Order   = [:type, :constant]
```

### Particle
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/abc/particle.jl"]
Order   = [:type, :constant, :function]
```

### Parse
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/abc/parse.jl"]
Order   = [:function]
```

### Properties
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/abc/properties.jl"]
Order   = [:function]
```

### Create
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/abc/create.jl"]
Order = [:function]
```

### Compute
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/abc/compute.jl"]
Order = [:function]
```

### Print
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/abc/print.jl"]
Order = [:function]
```

## QED-Model

### Feynman Diagrams
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/diagrams.jl"]
Order   = [:type, :function, :constant]
```

### Types
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/types.jl"]
Order   = [:type, :constant]
```

### Particle
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/particle.jl"]
Order   = [:type, :constant, :function]
```

### Parse
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/parse.jl"]
Order   = [:function]
```

### Properties
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/properties.jl"]
Order   = [:function]
```

### Create
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/create.jl"]
Order = [:function]
```

### Compute
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/compute.jl"]
Order = [:function]
```

### Print
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["models/qed/print.jl"]
Order = [:function]
```
