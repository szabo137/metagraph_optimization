
"""
    parse_process(string::AbstractString, model::QEDModel)

Parse a string representation of a process, such as "ke->ke" into the corresponding [`QEDProcessDescription`](@ref).
"""
function parse_process(str::AbstractString, model::QEDModel)
    inParticles = Dict{Type, Int}()
    outParticles = Dict{Type, Int}()

    if !(contains(str, "->"))
        throw("Did not find -> while parsing process \"$str\"")
    end

    (inStr, outStr) = split(str, "->")

    if (isempty(inStr) || isempty(outStr))
        throw("Process (\"$str\") input or output part is empty!")
    end

    for t in types(model)
        if (isincoming(t))
            inCount = count(x -> x == String(t)[1], inStr)

            if inCount != 0
                inParticles[t] = inCount
            end
        end
        if (isoutgoing(t))
            outCount = count(x -> x == String(t)[1], outStr)
            if outCount != 0
                outParticles[t] = outCount
            end
        end
    end

    if length(inStr) != sum(values(inParticles))
        throw("Encountered unknown characters in the input part of process \"$str\"")
    elseif length(outStr) != sum(values(outParticles))
        throw("Encountered unknown characters in the output part of process \"$str\"")
    end

    return QEDProcessDescription(inParticles, outParticles)
end
