using MetagraphOptimization
using QEDbase

import MetagraphOptimization.interaction_result

def_momentum = SFourMomentum(1.0, 0.0, 0.0, 0.0)

testparticleTypes = [ParticleA, ParticleB, ParticleC]
testparticles = [ParticleA(def_momentum), ParticleB(def_momentum), ParticleC(def_momentum)]

@testset "Interaction Result" begin
    for p1 in testparticleTypes, p2 in testparticleTypes
        if (p1 == p2)
            @test_throws AssertionError interaction_result(p1, p2)
        else
            @test interaction_result(p1, p2) == setdiff(testparticleTypes, [p1, p2])[1]
        end
    end
end

@testset "Vertex" begin
    @test isapprox(MetagraphOptimization.ABC_vertex(), 1 / 137.0)
end
