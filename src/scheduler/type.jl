using StaticArrays

"""
    FunctionCall{N}

Type representing a function call with `N` parameters. Contains the function to call, argument symbols, the return symbol and the device to execute on.
"""
struct FunctionCall{VectorType <: AbstractVector, M}
    func::Function
    arguments::VectorType
    additional_arguments::SVector{M, Any} # additional arguments (as values) for the function call, will be prepended to the other arguments
    return_symbol::Symbol
    device::AbstractDevice
end
