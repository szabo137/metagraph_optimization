using MetagraphOptimization

S1 = MetagraphOptimization.ComputeTaskABC_S1()
S2 = MetagraphOptimization.ComputeTaskABC_S2()
U = MetagraphOptimization.ComputeTaskABC_U()
V = MetagraphOptimization.ComputeTaskABC_V()
P = MetagraphOptimization.ComputeTaskABC_P()
Sum = MetagraphOptimization.ComputeTaskABC_Sum()

Data10 = MetagraphOptimization.DataTask(10)
Data20 = MetagraphOptimization.DataTask(20)

@test MetagraphOptimization.compute_effort(S1) == 11
@test MetagraphOptimization.compute_effort(S2) == 12
@test MetagraphOptimization.compute_effort(U) == 1
@test MetagraphOptimization.compute_effort(V) == 6
@test MetagraphOptimization.compute_effort(P) == 0
@test MetagraphOptimization.compute_effort(Sum) == 1
@test MetagraphOptimization.compute_effort(Data10) == 0
@test MetagraphOptimization.compute_effort(Data20) == 0

@test MetagraphOptimization.data(S1) == 0
@test MetagraphOptimization.data(S2) == 0
@test MetagraphOptimization.data(U) == 0
@test MetagraphOptimization.data(V) == 0
@test MetagraphOptimization.data(P) == 0
@test MetagraphOptimization.data(Sum) == 0
@test MetagraphOptimization.data(Data10) == 10
@test MetagraphOptimization.data(Data20) == 20

@test S1 != S2
@test Data10 != Data20

Data10_2 = MetagraphOptimization.DataTask(10)

# two data tasks with same data are identical, their nodes need not be
@test Data10_2 == Data10

@test Data10 == Data10
@test S1 == S1

Data10_3 = copy(Data10)

@test Data10_3 == Data10

S1_2 = copy(S1)

@test S1_2 == S1
@test S1 == MetagraphOptimization.ComputeTaskABC_S1()
