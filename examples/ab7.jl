using MetagraphOptimization
using BenchmarkTools

println("Getting machine info")
@time machine = get_machine_info()

println("Making model")
@time model = ABCModel()

println("Making process")
process_str = "AB->ABBBBBBB"
@time process = parse_process(process_str, model)

println("Parsing DAG")
@time graph = parse_dag("input/$process_str.txt", model)

println("Generating input data")
@time input_data = [gen_process_input(process) for _ in 1:1000]

println("Reducing graph")
@time optimize_to_fixpoint!(ReductionOptimizer(), graph)

println("Generating compute function")
@time compute_func = get_compute_function(graph, process, machine)

println("First run, single argument")
@time compute_func(input_data[1])

println("\nBenchmarking function, 1 input")
display(@benchmark compute_func($(input_data[1])))

println("\nBenchmarking function, 1000 inputs")
display(@benchmark compute_func.($input_data))
