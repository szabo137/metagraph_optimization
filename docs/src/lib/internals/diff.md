# Diff

## Type
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["diff/type.jl"]
Order   = [:type]
```

## Properties
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["diff/properties.jl"]
Order   = [:function]
```

## Printing
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["diff/print.jl"]
Order   = [:function]
```
