
"""
    show(io::IO, process::ABCProcessDescription)

Pretty print an [`ABCProcessDescription`](@ref) (no newlines).

```jldoctest
julia> using MetagraphOptimization

julia> print(parse_process("AB->ABBB", ABCModel()))
ABC Process: 'AB->ABBB'
```
"""
function show(io::IO, process::ABCProcessDescription)
    # types() gives the types in order (ABC) instead of random like keys() would
    print(io, "ABC Process: \'")
    for type in types(ABCModel())
        for _ in 1:get(process.inParticles, type, 0)
            print(io, String(type))
        end
    end
    print(io, "->")
    for type in types(ABCModel())
        for _ in 1:get(process.outParticles, type, 0)
            print(io, String(type))
        end
    end
    print(io, "'")
    return nothing
end

"""
    show(io::IO, processInput::ABCProcessInput)

Pretty print an [`ABCProcessInput`](@ref) (with newlines).
"""
function show(io::IO, processInput::ABCProcessInput)
    println(io, "Input for $(processInput.process):")
    println(io, "Incoming particles:")
    if !isempty(processInput.inA)
        println(io, "  $(processInput.inA)")
    end
    if !isempty(processInput.inB)
        println(io, "  $(processInput.inB)")
    end
    if !isempty(processInput.inC)
        println(io, "  $(processInput.inC)")
    end
    println(io, "Outgoing particles:")
    if !isempty(processInput.outA)
        println(io, "  $(processInput.outA)")
    end
    if !isempty(processInput.outB)
        println(io, "  $(processInput.outB)")
    end
    if !isempty(processInput.outC)
        println(io, "  $(processInput.outC)")
    end
end

"""
    show(io::IO, particle::T) where {T <: ABCParticle}

Pretty print an [`ABCParticle`](@ref) (no newlines).
"""
function show(io::IO, particle::T) where {T <: ABCParticle}
    print(io, "$(String(typeof(particle))): $(particle.momentum)")
    return nothing
end
