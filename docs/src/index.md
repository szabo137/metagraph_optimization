# MetagraphOptimization.jl

*A domain-specific DAG-optimizer*

## Package Features
- Read a DAG from a file
- Analyze its properties
- Mute the graph using the operations NodeFusion, NodeReduction and NodeSplit

## Coming Soon:
- Add Code Generation from finished DAG
- Add optimization algorithms and strategies

## Library Outline

```@contents
Pages = [
    "lib/public.md",
    "lib/internals.md"
]
```

### [Index](@id main-index)
```@index
Pages = ["lib/public.md"]
```
