# Properties

## Type
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["properties/type.jl"]
Order   = [:type]
```

## Create
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["properties/create.jl"]
Order   = [:function]
```

## Utility
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["properties/utility.jl"]
Order   = [:function]
```
