# Node

## Type
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["node/type.jl"]
Order   = [:type]
```

## Create
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["node/create.jl"]
Order   = [:function]
```

## Compare
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["node/compare.jl"]
Order   = [:function]
```

## Properties
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["node/properties.jl"]
Order   = [:function]
```

## Print
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["node/print.jl"]
Order   = [:function]
```

## Validate
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["node/validate.jl"]
Order   = [:function]
```
