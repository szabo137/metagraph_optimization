(AB->ABBBBBBB, 1)   1.620 s (5909018 allocations: 656.78 MiB)
(AB->ABBBBBBB, 2)   758.299 ms (5909088 allocations: 765.78 MiB)
(AB->ABBBBBBB, 3)   595.788 ms (5909161 allocations: 748.89 MiB)
(AB->ABBBBBBB, 4)   849.007 ms (5880250 allocations: 762.00 MiB)
(AB->ABBBBBBB, 5)   563.021 ms (5880332 allocations: 781.17 MiB)
(AB->ABBBBBBB, 6)   526.095 ms (5880419 allocations: 818.32 MiB)
(AB->ABBBBBBB, 7)   586.057 ms (5880482 allocations: 826.36 MiB)
(AB->ABBBBBBB, 8)   504.515 ms (5880542 allocations: 796.58 MiB)

(AB->ABBBBBBB, 1)   1.537 s (5596315 allocations: 616.81 MiB)
(AB->ABBBBBBB, 2)   826.918 ms (5596385 allocations: 725.81 MiB)
(AB->ABBBBBBB, 3)   538.787 ms (5596457 allocations: 708.92 MiB)
(AB->ABBBBBBB, 4)   918.853 ms (5596528 allocations: 725.08 MiB)
(AB->ABBBBBBB, 5)   511.959 ms (5596606 allocations: 744.25 MiB)
(AB->ABBBBBBB, 6)   887.160 ms (5596691 allocations: 763.42 MiB)
(AB->ABBBBBBB, 7)   898.757 ms (5596762 allocations: 789.91 MiB)
(AB->ABBBBBBB, 8)   497.545 ms (5596820 allocations: 759.66 MiB)


Initial:

$ julia --project=examples/ -e 'using BenchmarkTools; using MetagraphOptimization; parse_abc("input/AB->AB.txt"); @time g = parse_abc("input/AB->ABBBBBBBBB.txt")'
 65.370947 seconds (626.10 M allocations: 37.381 GiB, 53.59% gc time, 0.01% compilation time)

Removing make_edge from calls in parse:
 50.053920 seconds (593.41 M allocations: 32.921 GiB, 49.70% gc time, 0.09% compilation time)

Nodes operation storage rework (and O3):
 31.997128 seconds (450.66 M allocations: 25.294 GiB, 31.56% gc time, 0.14% compilation time)
 