using MetagraphOptimization
using BenchmarkTools
using Base
using Base.Filesystem

function bench_txt(filepath::String, bench::Bool = true)
    name = basename(filepath)
    name, _ = splitext(name)

    filepath = joinpath(@__DIR__, "../input/", filepath)
    if !isfile(filepath)
        println("File ", filepath, " does not exist, skipping bench")
        return
    end

    model = ABCModel()

    println(name, ":")
    g = parse_dag(filepath, model)
    print(g)
    println("  Graph size in memory: ", bytes_to_human_readable(MetagraphOptimization.mem(g)))

    if (bench)
        @btime parse_dag($filepath, $model)
    end

    println("  Get Operations: ")
    @time get_operations(g)
    return println()
end

function import_bench()
    bench_txt("AB->AB.txt")
    bench_txt("AB->ABBB.txt")
    bench_txt("AB->ABBBBB.txt")
    bench_txt("AB->ABBBBBBB.txt")
    #bench_txt("AB->ABBBBBBBBB.txt")
    bench_txt("ABAB->ABAB.txt")
    return bench_txt("ABAB->ABC.txt")
end

import_bench()
