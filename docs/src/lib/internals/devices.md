# Devices

## Interface
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/interface.jl"]
Order = [:type, :constant, :function]
```

## Detect
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/detect.jl"]
Order = [:function]
```

## Measure
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/measure.jl"]
Order = [:function]
```

## Implementations

### General
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/impl.jl"]
Order = [:type, :function]
```

### NUMA
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/numa/impl.jl"]
Order = [:type, :function]
```

### CUDA
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/cuda/impl.jl"]
Order = [:type, :function]
```

### ROCm
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/rocm/impl.jl"]
Order = [:type, :function]
```

### oneAPI
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["devices/oneapi/impl.jl"]
Order = [:type, :function]
```
