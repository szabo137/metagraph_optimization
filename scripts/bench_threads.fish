#!/bin/fish
set minthreads 1
set maxthreads 8

julia --project=./examples -t 4 -e 'import Pkg; Pkg.instantiate()'

#for i in $(seq $minthreads $maxthreads)
#   printf "(AB->AB, $i) "
#   julia --project=./examples -t $i -O3 -e 'using MetagraphOptimization; using BenchmarkTools; @btime get_operations(graph) setup=(graph = parse_dag("input/AB->AB.txt"), ABCModel())'
#end

#for i in $(seq $minthreads $maxthreads)
#   printf "(AB->ABBB, $i) "
#   julia --project=./examples -t $i -O3 -e 'using MetagraphOptimization; using BenchmarkTools; @btime get_operations(graph) setup=(graph = parse_dag("input/AB->ABBB.txt"), ABCModel())'
#end

#for i in $(seq $minthreads $maxthreads)
#   printf "(AB->ABBBBB, $i) "
#   julia --project=./examples -t $i -O3 -e 'using MetagraphOptimization; using BenchmarkTools; @btime get_operations(graph) setup=(graph = parse_dag("input/AB->ABBBBB.txt"), ABCModel())'
#end

for i in $(seq $minthreads $maxthreads)
   printf "(AB->ABBBBBBB, $i) "
   julia --project=./examples -t $i -O3 -e 'using MetagraphOptimization; using BenchmarkTools; @btime get_operations(graph) setup=(graph = parse_dag("input/AB->ABBBBBBB.txt"), ABCModel())'
end
