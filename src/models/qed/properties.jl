# TODO use correct numbers

"""
    compute_effort(t::ComputeTaskQED_S1)

Return the compute effort of an S1 task.
"""
compute_effort(t::ComputeTaskQED_S1)::Float64 = 11.0

"""
    compute_effort(t::ComputeTaskQED_S2)

Return the compute effort of an S2 task.
"""
compute_effort(t::ComputeTaskQED_S2)::Float64 = 12.0

"""
    compute_effort(t::ComputeTaskQED_U)

Return the compute effort of a U task.
"""
compute_effort(t::ComputeTaskQED_U)::Float64 = 1.0

"""
    compute_effort(t::ComputeTaskQED_V)

Return the compute effort of a V task.
"""
compute_effort(t::ComputeTaskQED_V)::Float64 = 6.0

"""
    compute_effort(t::ComputeTaskQED_P)

Return the compute effort of a P task.
"""
compute_effort(t::ComputeTaskQED_P)::Float64 = 0.0

"""
    compute_effort(t::ComputeTaskQED_Sum)

Return the compute effort of a Sum task. 

Note: This is a constant compute effort, even though sum scales with the number of its inputs. Since there is only ever a single sum node in a graph generated from the QED-Model,
this doesn't matter.
"""
compute_effort(t::ComputeTaskQED_Sum)::Float64 = 1.0

"""
    children(::ComputeTaskQED_S1)

Return the number of children of a ComputeTaskQED_S1 (always 1).
"""
children(::ComputeTaskQED_S1) = 1

"""
    children(::ComputeTaskQED_S2)

Return the number of children of a ComputeTaskQED_S2 (always 2).
"""
children(::ComputeTaskQED_S2) = 2

"""
    children(::ComputeTaskQED_P)

Return the number of children of a ComputeTaskQED_P (always 1).
"""
children(::ComputeTaskQED_P) = 1

"""
    children(::ComputeTaskQED_U)

Return the number of children of a ComputeTaskQED_U (always 1).
"""
children(::ComputeTaskQED_U) = 1

"""
    children(::ComputeTaskQED_V)

Return the number of children of a ComputeTaskQED_V (always 2).
"""
children(::ComputeTaskQED_V) = 2

"""
    children(::ComputeTaskQED_Sum)

Return the number of children of a ComputeTaskQED_Sum.
"""
children(t::ComputeTaskQED_Sum) = t.children_number

function add_child!(t::ComputeTaskQED_Sum)
    t.children_number += 1
    return nothing
end
