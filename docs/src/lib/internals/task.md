# Task

## Type
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["task/type.jl"]
Order   = [:type]
```

## Create
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["task/create.jl"]
Order   = [:function]
```

## Compare
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["task/compare.jl"]
Order   = [:function]
```

## Compute
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["task/compute.jl"]
Order   = [:function]
```

## Properties
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["task/properties.jl"]
Order   = [:function]
```
