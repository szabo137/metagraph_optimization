# Optimization

## Interface

The interface that has to be implemented for an optimization algorithm.

```@autodocs
Modules = [MetagraphOptimization]
Pages = ["optimization/interafce.jl"]
Order = [:type, :constant, :function]
```

## Random Walk Optimizer

Implementation of a random walk algorithm.

```@autodocs
Modules = [MetagraphOptimization]
Pages = ["estimator/random_walk.jl"]
Order = [:type, :function]
```

## Reduction Optimizer

Implementation of a an optimizer that reduces as far as possible.

```@autodocs
Modules = [MetagraphOptimization]
Pages = ["estimator/reduce.jl"]
Order = [:type, :function]
```

## Greedy Optimizer

Implementation of a greedy optimization algorithm.

```@autodocs
Modules = [MetagraphOptimization]
Pages = ["estimator/greedy.jl"]
Order = [:type, :function]
```
