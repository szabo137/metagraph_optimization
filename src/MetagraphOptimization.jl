"""
    MetagraphOptimization

A module containing tools to work on DAGs.
"""
module MetagraphOptimization

using QEDbase

# graph types
export DAG
export Node
export Edge
export ComputeTaskNode
export DataTaskNode
export AbstractTask
export AbstractComputeTask
export AbstractDataTask
export DataTask
export FusedComputeTask
export PossibleOperations
export GraphProperties

# graph functions
export make_node
export make_edge
export insert_node
export insert_edge
export is_entry_node
export is_exit_node
export parents
export children
export compute
export data
export compute_effort
export task
export get_properties
export get_exit_node
export operation_stack_length
export is_valid, is_scheduled

# graph operation related
export Operation
export AppliedOperation
export NodeFusion
export NodeReduction
export NodeSplit
export push_operation!
export pop_operation!
export can_pop
export reset_graph!
export get_operations

# ABC model
export ParticleValue
export ParticleA, ParticleB, ParticleC
export ABCParticle, ABCProcessDescription, ABCProcessInput, ABCModel
export ComputeTaskABC_P
export ComputeTaskABC_S1
export ComputeTaskABC_S2
export ComputeTaskABC_V
export ComputeTaskABC_U
export ComputeTaskABC_Sum

# QED model
export FeynmanDiagram, FeynmanVertex, FeynmanTie, FeynmanParticle
export PhotonStateful, FermionStateful, AntiFermionStateful
export QEDParticle, QEDProcessDescription, QEDProcessInput, QEDModel
export ComputeTaskQED_P
export ComputeTaskQED_S1
export ComputeTaskQED_S2
export ComputeTaskQED_V
export ComputeTaskQED_U
export ComputeTaskQED_Sum
export gen_graph

# code generation related
export execute
export parse_dag, parse_process
export gen_process_input
export get_compute_function
export gen_tape, execute_tape

# estimator
export cost_type, graph_cost, operation_effect
export GlobalMetricEstimator, CDCost

# optimization
export AbstractOptimizer, GreedyOptimizer, ReductionOptimizer, RandomWalkOptimizer
export optimize_step!, optimize!
export fixpoint_reached, optimize_to_fixpoint!

# machine info
export Machine
export get_machine_info

export ==, in, show, isempty, delete!, length

export bytes_to_human_readable

# TODO: this is probably not good
import QEDprocesses.compute

import Base.length
import Base.show
import Base.==
import Base.+
import Base.-
import Base.in
import Base.copy
import Base.isempty
import Base.delete!
import Base.insert!
import Base.collect


include("devices/interface.jl")
include("task/type.jl")
include("node/type.jl")
include("diff/type.jl")
include("properties/type.jl")
include("operation/type.jl")
include("graph/type.jl")
include("scheduler/type.jl")

include("trie.jl")
include("utility.jl")

include("diff/print.jl")
include("diff/properties.jl")

include("graph/compare.jl")
include("graph/interface.jl")
include("graph/mute.jl")
include("graph/print.jl")
include("graph/properties.jl")
include("graph/validate.jl")

include("node/compare.jl")
include("node/create.jl")
include("node/print.jl")
include("node/properties.jl")
include("node/validate.jl")

include("operation/utility.jl")
include("operation/iterate.jl")
include("operation/apply.jl")
include("operation/clean.jl")
include("operation/find.jl")
include("operation/get.jl")
include("operation/print.jl")
include("operation/validate.jl")

include("properties/create.jl")
include("properties/utility.jl")

include("task/create.jl")
include("task/compare.jl")
include("task/compute.jl")
include("task/properties.jl")

include("estimator/interface.jl")
include("estimator/global_metric.jl")

include("optimization/interface.jl")
include("optimization/greedy.jl")
include("optimization/random_walk.jl")
include("optimization/reduce.jl")

include("models/interface.jl")
include("models/print.jl")

include("models/abc/types.jl")
include("models/abc/particle.jl")
include("models/abc/compute.jl")
include("models/abc/create.jl")
include("models/abc/properties.jl")
include("models/abc/parse.jl")
include("models/abc/print.jl")

include("models/qed/types.jl")
include("models/qed/particle.jl")
include("models/qed/diagrams.jl")
include("models/qed/compute.jl")
include("models/qed/create.jl")
include("models/qed/properties.jl")
include("models/qed/parse.jl")
include("models/qed/print.jl")

include("devices/measure.jl")
include("devices/detect.jl")
include("devices/impl.jl")

include("devices/numa/impl.jl")
include("devices/cuda/impl.jl")
# can currently not use AMDGPU because of incompatability with the newest rocm drivers
# include("devices/rocm/impl.jl")
# oneapi seems also broken for now
# include("devices/oneapi/impl.jl")

include("scheduler/interface.jl")
include("scheduler/greedy.jl")

include("code_gen/type.jl")
include("code_gen/tape_machine.jl")
include("code_gen/function.jl")

end # module MetagraphOptimization
