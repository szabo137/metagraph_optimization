using MetagraphOptimization

nC1 = MetagraphOptimization.make_node(MetagraphOptimization.ComputeTaskABC_U())
nC2 = MetagraphOptimization.make_node(MetagraphOptimization.ComputeTaskABC_V())
nC3 = MetagraphOptimization.make_node(MetagraphOptimization.ComputeTaskABC_P())
nC4 = MetagraphOptimization.make_node(MetagraphOptimization.ComputeTaskABC_Sum())

nD1 = MetagraphOptimization.make_node(MetagraphOptimization.DataTask(10))
nD2 = MetagraphOptimization.make_node(MetagraphOptimization.DataTask(20))

@test_throws ErrorException MetagraphOptimization.make_edge(nC1, nC2)
@test_throws ErrorException MetagraphOptimization.make_edge(nC1, nC1)
@test_throws ErrorException MetagraphOptimization.make_edge(nC3, nC4)
@test_throws ErrorException MetagraphOptimization.make_edge(nD1, nD2)
@test_throws ErrorException MetagraphOptimization.make_edge(nD1, nD1)

ed1 = MetagraphOptimization.make_edge(nC1, nD1)
ed2 = MetagraphOptimization.make_edge(nD1, nC2)
ed3 = MetagraphOptimization.make_edge(nC2, nD2)
ed4 = MetagraphOptimization.make_edge(nD2, nC3)

@test nC1 != nC2
@test nD1 != nD2
@test nC1 != nD1
@test nC3 != nC4

nC1_2 = copy(nC1)
@test nC1_2 != nC1

nD1_2 = copy(nD1)
@test nD1_2 != nD1

nD1_c = MetagraphOptimization.make_node(MetagraphOptimization.DataTask(10))
@test nD1_c != nD1
