"""
    copy(t::AbstractDataTask)

Fallback implementation of the copy of an abstract data task, throwing an error.
"""
copy(t::AbstractDataTask) = error("Need to implement copying for your data tasks!")

"""
    copy(t::AbstractComputeTask)

Return a copy of the given compute task.
"""
copy(t::AbstractComputeTask) = typeof(t)()

"""
    copy(t::FusedComputeTask)

Return a copy of th egiven [`FusedComputeTask`](@ref).
"""
function copy(t::FusedComputeTask)
    return FusedComputeTask(copy(t.first_task), copy(t.second_task), copy(t.t1_inputs), t.t1_output, copy(t.t2_inputs))
end

function FusedComputeTask(
    T1::Type{<:AbstractComputeTask},
    T2::Type{<:AbstractComputeTask},
    t1_inputs::Vector{String},
    t1_output::String,
    t2_inputs::Vector{String},
)
    return FusedComputeTask(T1(), T2(), t1_inputs, t1_output, t2_inputs)
end
