using MetagraphOptimization
using Plots
using Random

function gen_plot(filepath)
    name = basename(filepath)
    name, _ = splitext(name)

    filepath = joinpath(@__DIR__, "../input/", filepath)
    if !isfile(filepath)
        println("File ", filepath, " does not exist, skipping")
        return
    end

    g = parse_dag(filepath, ABCModel())

    Random.seed!(1)

    println("Random Walking... ")

    x = Vector{Float64}()
    y = Vector{Float64}()

    for i in 1:30
        print("\r", i)
        # push
        opt = get_operations(g)

        # choose one of fuse/split/reduce
        option = rand(1:3)
        if option == 1 && !isempty(opt.nodeFusions)
            push_operation!(g, rand(collect(opt.nodeFusions)))
            println("NF")
        elseif option == 2 && !isempty(opt.nodeReductions)
            push_operation!(g, rand(collect(opt.nodeReductions)))
            println("NR")
        elseif option == 3 && !isempty(opt.nodeSplits)
            push_operation!(g, rand(collect(opt.nodeSplits)))
            println("NS")
        else
            i = i - 1
        end

        props = get_properties(g)
        push!(x, props.data)
        push!(y, props.computeEffort)
    end

    println("\rDone.")

    plot([x[1], x[2]], [y[1], y[2]], linestyle = :solid, linewidth = 1, color = :red, legend = false)
    # Create lines connecting the reference point to each data point
    for i in 3:length(x)
        plot!([x[i - 1], x[i]], [y[i - 1], y[i]], linestyle = :solid, linewidth = 1, color = :red)
    end

    return gui()
end

gen_plot("AB->ABBB.txt")
