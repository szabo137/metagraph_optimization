#!/bin/fish

function printhelp
    echo "Give an interaction like \"AB->AB\" you want to generate the graph for"
    exit 1
end

if test $(count $argv) -ne 1
    printhelp
end

set OutFile "$argv.txt"

set inout $(string split -- "->" $argv)

if test $(count $inout) -ne 2
    echo "Couldn't parse string into in and out particles"
    printhelp
end

set PythonCommand "import FeynmanDAG"

set particlesIn $(string split '' $inout[1])
set particlesOut $(string split '' $inout[2])

set ACount 0
set BCount 0
set CCount 0

for i in $(seq $(count $particlesIn))
    switch $particlesIn[$i]
    case A
        set ACount $(math $ACount + 1)
        set name "\"$particlesIn[$i]$ACount\""
    case B
        set BCount $(math $BCount + 1)
        set name "\"$particlesIn[$i]$BCount\""
    case C
        set CCount $(math $CCount + 1)
        set name "\"$particlesIn[$i]$CCount\""
    case '*'
        echo "Encountered unknown particle $particlesIn[$i]"
        printhelp
    end
    set particlesIn[$i] "FeynmanDAG.Particle$particlesIn[$i]($name, True, True)"
end

for i in $(seq $(count $particlesOut))
    switch $particlesOut[$i]
    case A
        set ACount $(math $ACount + 1)
        set name "\"$particlesOut[$i]$ACount\""
    case B
        set BCount $(math $BCount + 1)
        set name "\"$particlesOut[$i]$BCount\""
    case C
        set CCount $(math $CCount + 1)
        set name "\"$particlesOut[$i]$CCount\""
    case '*'
        echo "Encountered unknown particle $particlesOut[$i]"
        printhelp
    end
    set particlesOut[$i] "FeynmanDAG.Particle$particlesOut[$i]($name, False, True)"
end

set ParticlesList "["
for i in $(seq $(count $particlesIn))
    if test $i -ne 1
        set ParticlesList "$ParticlesList, "
    end
    set ParticlesList "$ParticlesList$particlesIn[$i]"
end
for i in $(seq $(count $particlesOut))
    set ParticlesList "$ParticlesList, $particlesOut[$i]"
end
set ParticlesList "$ParticlesList]"

set PythonCommand "$PythonCommand;particles = $ParticlesList"
set PythonCommand "$PythonCommand;g = FeynmanDAG.ComputeGraph.generate(particles)"
set PythonCommand "$PythonCommand;d = g.DAG_generator()"
set PythonCommand "$PythonCommand;outfile = open(\"$OutFile\", \"w\")"
set PythonCommand "$PythonCommand;outfile.write(str(d.nodes()) + \"\\\n\")"
set PythonCommand "$PythonCommand;outfile.write(str(d.edges()) + \"\\\n\")"
set PythonCommand "$PythonCommand;outfile.close()"

echo "PythonCommand: $PythonCommand"

echo -e "$PythonCommand" | python
