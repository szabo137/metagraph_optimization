# Operation

## Types
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/type.jl"]
Order   = [:type]
```

## Find
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/find.jl"]
Order   = [:function]
```

## Apply
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/apply.jl"]
Order   = [:function]
```

## Get
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/get.jl"]
Order   = [:function]
```

## Clean
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/clean.jl"]
Order   = [:function]
```

## Utility
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/utility.jl"]
Order   = [:function]
```

## Print
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/print.jl"]
Order   = [:function]
```

## Validate
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["operation/validate.jl"]
Order   = [:function]
```
