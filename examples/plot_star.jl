using MetagraphOptimization
using Plots
using Random

function gen_plot(filepath)
    name = basename(filepath)
    name, _ = splitext(name)

    filepath = joinpath(@__DIR__, "../input/", filepath)
    if !isfile(filepath)
        println("File ", filepath, " does not exist, skipping")
        return
    end

    g = parse_dag(filepath, ABCModel())

    Random.seed!(1)

    println("Random Walking... ")

    for i in 1:30
        print("\r", i)
        # push
        opt = get_operations(g)

        # choose one of fuse/split/reduce
        option = rand(1:3)
        if option == 1 && !isempty(opt.nodeFusions)
            push_operation!(g, rand(collect(opt.nodeFusions)))
            println("NF")
        elseif option == 2 && !isempty(opt.nodeReductions)
            push_operation!(g, rand(collect(opt.nodeReductions)))
            println("NR")
        elseif option == 3 && !isempty(opt.nodeSplits)
            push_operation!(g, rand(collect(opt.nodeSplits)))
            println("NS")
        else
            i = i - 1
        end
    end

    println("\rDone.")




    props = get_properties(g)
    x0 = props.data
    y0 = props.computeEffort

    x = Vector{Float64}()
    y = Vector{Float64}()
    names = Vector{String}()

    opt = get_operations(g)
    for op in opt.nodeFusions
        push_operation!(g, op)
        props = get_properties(g)
        push!(x, props.data)
        push!(y, props.computeEffort)
        pop_operation!(g)

        push!(names, "NF: (" * string(props.data) * ", " * string(props.computeEffort) * ")")
    end
    for op in opt.nodeReductions
        push_operation!(g, op)
        props = get_properties(g)
        push!(x, props.data)
        push!(y, props.computeEffort)
        pop_operation!(g)

        push!(names, "NR: (" * string(props.data) * ", " * string(props.computeEffort) * ")")
    end
    for op in opt.nodeSplits
        push_operation!(g, op)
        props = get_properties(g)
        push!(x, props.data)
        push!(y, props.computeEffort)
        pop_operation!(g)

        push!(names, "NS: (" * string(props.data) * ", " * string(props.computeEffort) * ")")
    end

    plot([x0, x[1]], [y0, y[1]], linestyle = :solid, linewidth = 1, color = :red, legend = false)
    # Create lines connecting the reference point to each data point
    for i in 2:length(x)
        plot!([x0, x[i]], [y0, y[i]], linestyle = :solid, linewidth = 1, color = :red)
    end
    #scatter!(x, y, label=names)

    print(names)

    return gui()
end

gen_plot("AB->ABBB.txt")
