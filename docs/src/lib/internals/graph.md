# Graph

## Type
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["graph/type.jl"]
Order   = [:type]
```

## Interface
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["graph/interface.jl"]
Order   = [:function]
```

## Compare
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["graph/compare.jl"]
Order   = [:function]
```

## Mute
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["graph/mute.jl"]
Order   = [:function]
```

## Print
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["graph/print.jl"]
Order   = [:function]
```

## Properties
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["graph/properties.jl"]
Order   = [:function]
```

## Validate
```@autodocs
Modules = [MetagraphOptimization]
Pages = ["graph/validate.jl"]
Order   = [:function]
```
