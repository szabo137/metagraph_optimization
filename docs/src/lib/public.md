# Public Documentation

Documentation for `MetagraphOptimization.jl`'s public interface.

See the Internals section of the manual for documentation of everything else.

```@autodocs
Modules = [MetagraphOptimization]
Pages   = ["MetagraphOptimization.jl"]
Order   = [:module]
```

## Contents

```@contents
Pages = ["public.md"]
Depth = 2
```

## Index

```@index
Pages = ["public.md"]
```
